#import time, signal, sys, ssl, logging
import time
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer

#logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

class SimpleEcho(WebSocket):
    
    def handleMessage(self):
        if self.data is None:
            self.data = ''
        
        
#        if self.data is 'ip'
#            print self.server.connections
        
        print time.asctime(time.localtime()), self.data
        # echo message back to client
        #self.sendMessage(str(self.data))
        
    def handleConnected(self):
        print time.asctime(time.localtime()), self.address, 'connected'
              
    def handleClose(self):
        print time.asctime(time.localtime()), self.address, 'closed'

server = SimpleWebSocketServer('', 8080, SimpleEcho)
server.serveforever()
    
